package edu.uoc.pfp.crowdsourcing;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Fecha {
    private final Date date;
    private static final SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    public Fecha(Date date) {
        if (date == null) throw new IllegalArgumentException("Fecha Java obligatoria");
        this.date = date;
    }

    public boolean mayorOIgualQue(Fecha f){
        if(f == null) throw new IllegalArgumentException("Fecha obligatoria");
        return date.compareTo(f.date) >= 0;
    }

    private static final long MS_POR_DIA = 24*3600*1000;

    public int diasHasta(Fecha f){
        long ms = f.date.getTime() - date.getTime();
        if(ms <= 0) return 0;
        long dias = ms / MS_POR_DIA;
        return Math.round(dias) + (dias - Math.round(dias) > 0?1:0);
    }

    @Override
    public String toString() {
        return df.format(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Fecha fecha = (Fecha) o;

        if (!date.equals(fecha.date)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return date.hashCode();
    }
}
