package edu.uoc.pfp.crowdsourcing;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Proyecto {
    private final IdProyecto id;
    private final Usuario autor;
    private String titulo;
    private String descripcion;
    private final Dinero objetivo;
    private final Fecha fechaInicio;
    private final Fecha fechaFin;
    private final Set<Recompensa> recompensas = new HashSet<Recompensa>();
    private final List<Articulo> entradasBlog = new LinkedList<Articulo>();

    Proyecto(Usuario autor, Dinero objetivo, Fecha fechaInicio, Fecha fechaFin, String titulo, String descripcion) {
        if(autor==null) throw new IllegalArgumentException("Autor obligatorio");
        if(objetivo==null) throw new IllegalArgumentException("Objetivo obligatorio");
        if(fechaInicio==null) throw new IllegalArgumentException("Fecha inicio obligatoria");
        if(getFechaFin()==null) throw new IllegalArgumentException("Fecha fin obligatoria");
        if(titulo==null) throw new IllegalArgumentException("Título obligatorio");
        if(descripcion==null) throw new IllegalArgumentException("Descripción obligatoria");
        int dias = fechaInicio.diasHasta(fechaFin);
        if(dias < 30 || dias > 60) throw new IllegalArgumentException("La fecha de fin debe ser entre 30 y 60 días posterior a la fecha de fin");
        this.id = new IdProyecto();
        this.autor = autor;
        this.objetivo = objetivo;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.titulo = titulo;
        this.descripcion = descripcion;
    }

    public Dinero getConseguido(){
        Dinero total = new Dinero(0,objetivo.getMoneda());
        for(Recompensa r : recompensas){
            total = total.suma(r.getConseguido());
        }
        return total;
    }

    public boolean getExito(){
        return getConseguido().mayorOIgualQue(objetivo);
    }

    public int getDiasRestantes(Fecha f){
        if(f == null) throw new IllegalArgumentException("Fecha obligatoria");
        return f.diasHasta(fechaFin);
    }

    public boolean getFinalizado(Fecha f){
        if(f == null) throw new IllegalArgumentException("Fecha obligatoria");
        return getDiasRestantes(f) == 0;
    }

    public int getNumMecenas(){
        int resultado = 0;
        for(Recompensa r : recompensas){
            resultado += r.getAportaciones().size();
        }
        return resultado;
    }

    public void editar(Usuario usuario, String nuevoTitulo, String nuevaDescripcion){
        if(usuario != autor) throw new RuntimeException("Usuario no autorizado");
        this.titulo = nuevoTitulo;
        this.descripcion = nuevaDescripcion;
    }

    public void nuevaEntradaBlog(String titulo, Texto texto, Fecha fecha){
        entradasBlog.add(new Articulo(titulo,fecha,texto));
    }

    private Recompensa getRecompensa(Dinero cantidad){
        for(Recompensa r : recompensas){
            if(r.getCantidad().equals(cantidad)){
                return r;
            }
        }
        return null;
    }

    public void aportar(Usuario mecenas, Dinero cantidad, Fecha fecha){
        if(mecenas == null) throw new IllegalArgumentException("Mecenas obligatorio");
        if(fecha == null) throw new IllegalArgumentException("Fecha obligatoria");
        Recompensa r = getRecompensa(cantidad);
        if(r == null) throw new IllegalArgumentException("La cantidad debe coincidir con una recompensa definida");
        r.aportar(mecenas, fecha);
    }

    public IdProyecto getId(){
        return id;
    }

    public Usuario getAutor() {
        return autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public Dinero getObjetivo() {
        return objetivo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public List<Articulo> getEntradasBlog() {
        return entradasBlog;
    }

    public Set<Recompensa> getRecompensas() {
        return recompensas;
    }

    public Fecha getFechaInicio() {
        return fechaInicio;
    }

    public Fecha getFechaFin() {
        return fechaFin;
    }
}
