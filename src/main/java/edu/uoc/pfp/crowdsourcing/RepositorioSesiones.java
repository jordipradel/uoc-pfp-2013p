package edu.uoc.pfp.crowdsourcing;

import java.util.HashMap;
import java.util.Map;

public class RepositorioSesiones {

    private final Map<IdSesion,Usuario> sesiones = new HashMap<IdSesion, Usuario>();

    public IdSesion nuevaSesion(Usuario usuario) {
        IdSesion idSesion = new IdSesion();
        sesiones.put(idSesion,usuario);
        return idSesion;
    }

    /** Returna al usuario identificado en la sesión idSesion o null si no existe tal sesión */
    public Usuario getUsuario(IdSesion idSesion) {
        return sesiones.get(idSesion);
    }

    /** Elimina la sesión identificada por idSesion */
    public void eliminar(IdSesion idSesion) {
        sesiones.remove(idSesion);
    }
}
