package edu.uoc.pfp.crowdsourcing;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RepositorioProyectos {

    private Map<IdProyecto,Proyecto> proyectosPorId = new HashMap<IdProyecto,Proyecto>();

    /** Retorna el proyecto identificado por id o null si no existe tal proyecto */
    public Proyecto getProyecto(IdProyecto id){
        return proyectosPorId.get(id);
    }

    /** Da de alta un nuevo proyecto con los datos indicados */
    public Proyecto nuevoProyecto(Usuario autor, Dinero objetivo, Fecha fechaInicio, Fecha fechaFin, String titulo, String descripcion){
        Proyecto p = new Proyecto(autor,objetivo,fechaInicio,fechaFin,titulo,descripcion);
        proyectosPorId.put(p.getId(),p);
        return p;
    }

    /** Retorna la lista de todos los proyectos */
    public List<Proyecto> listarProyectos(){
        return new LinkedList<Proyecto>(proyectosPorId.values());
    }




}
