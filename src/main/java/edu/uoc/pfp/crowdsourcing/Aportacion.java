package edu.uoc.pfp.crowdsourcing;

public class Aportacion {

    private final Recompensa recompensa;
    private final Usuario mecenas;
    private final Fecha fecha;

    public Aportacion(Recompensa recompensa, Usuario mecenas, Fecha fecha) {
        if(recompensa==null) throw new IllegalArgumentException("Recompensa obligatoria");
        if(mecenas==null) throw new IllegalArgumentException("Mecenas obligatorio");
        if(fecha==null) throw new IllegalArgumentException("Fecha obigatoria");
        this.mecenas = mecenas;
        this.recompensa = recompensa;
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Aportacion de " + mecenas + " por " + recompensa.getCantidad();
    }

    public Usuario getMecenas() {
        return mecenas;
    }

    public Recompensa getRecompensa() {
        return recompensa;
    }

    public Fecha getFecha() {
        return fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Aportacion that = (Aportacion) o;

        if (!fecha.equals(that.fecha)) return false;
        if (!mecenas.equals(that.mecenas)) return false;
        if (!recompensa.equals(that.recompensa)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mecenas.hashCode();
        result = 31 * result + recompensa.hashCode();
        result = 31 * result + fecha.hashCode();
        return result;
    }
}
