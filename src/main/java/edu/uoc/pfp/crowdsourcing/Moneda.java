package edu.uoc.pfp.crowdsourcing;

import java.text.DecimalFormat;

public class Moneda {
    private final String simboloISO;
    private final String simbolo;
    private final int numDecimales;

    public Moneda(String simboloISO, String simbolo, int numDecimales) {
        if(simboloISO==null) throw new IllegalArgumentException("Símbolo ISO incorrecto");
        if(simbolo==null || simbolo.length() > 2) throw new IllegalArgumentException("Símbolo incorrecto");
        if(numDecimales <0 || numDecimales > 4) throw new IllegalArgumentException("Número de decimales incorrecto");
        this.numDecimales = numDecimales;
        this.simboloISO = simboloISO;
        this.simbolo = simbolo;
    }

    public String toString(){
        return simbolo;
    }

    public int getEscala() {
        long result = Math.round(Math.pow(10.0,numDecimales));
        return (int) result;
    }

    public String format(int cantidadEnCentimos){
        double cantidad = cantidadEnCentimos * 1.0 / getEscala();
        return new DecimalFormat("#.##").format(cantidad) + getSimbolo();
    }

    public String getSimboloISO() {
        return simboloISO;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public int getNumDecimales() {
        return numDecimales;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Moneda moneda = (Moneda) o;

        if (numDecimales != moneda.numDecimales) return false;
        if (!simbolo.equals(moneda.simbolo)) return false;
        if (!simboloISO.equals(moneda.simboloISO)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = simboloISO.hashCode();
        result = 31 * result + simbolo.hashCode();
        result = 31 * result + numDecimales;
        return result;
    }
}
