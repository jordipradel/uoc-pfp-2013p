package edu.uoc.pfp.crowdsourcing;

public class Id {

    private final String id;

    public Id(){
        this.id = UtilidadesId.nuevoId();
    }

    public Id(String id) {
        if(id == null) throw new IllegalArgumentException();
        this.id = id;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Id that = (Id) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
