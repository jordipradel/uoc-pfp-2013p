package edu.uoc.pfp.crowdsourcing;

import java.util.LinkedList;
import java.util.List;

public class Recompensa {
    private final Dinero cantidad;
    private final Integer limiteAportaciones;
    private final String descripcionRecompensa;
    private final List<Aportacion> aportaciones;

    public Recompensa(Dinero cantidad, Integer limiteAportaciones, String descripcionRecompensa) {
        this.cantidad = cantidad;
        this.limiteAportaciones = limiteAportaciones;
        this.descripcionRecompensa = descripcionRecompensa;
        this.aportaciones = new LinkedList<Aportacion>();
    }

    public Dinero getConseguido() {
        return new Dinero(0,cantidad.getMoneda()).multiplica(aportaciones.size());
    }

    public void aportar(Usuario mecenas, Fecha fecha) {
        if(getNumeroAportaciones() == limiteAportaciones) throw new IllegalStateException("Se ha alcanzado el límite de aportaciones");
        aportaciones.add(new Aportacion(this,mecenas,fecha));
    }

    public int getNumeroAportaciones(){
        return aportaciones.size();
    }

    @Override
    public String toString() {
        return "Aportando " + cantidad;
    }

    public Dinero getCantidad() {
        return cantidad;
    }

    public Integer getLimiteAportaciones() {
        return limiteAportaciones;
    }

    public String getDescripcionRecompensa() {
        return descripcionRecompensa;
    }

    public List<Aportacion> getAportaciones() {
        return aportaciones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recompensa that = (Recompensa) o;

        if (!cantidad.equals(that.cantidad)) return false;
        if (!descripcionRecompensa.equals(that.descripcionRecompensa)) return false;
        if (!limiteAportaciones.equals(that.limiteAportaciones)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cantidad.hashCode();
        result = 31 * result + limiteAportaciones.hashCode();
        result = 31 * result + descripcionRecompensa.hashCode();
        return result;
    }

}
