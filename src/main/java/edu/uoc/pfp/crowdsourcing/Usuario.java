package edu.uoc.pfp.crowdsourcing;

public class Usuario {
    private final String nombreUsuario;
    private final String email;
    private String contrasena;
    private String perfil;

    public Usuario(String nombreUsuario, String email, String contrasena) {
        if(nombreUsuario == null)  throw new IllegalArgumentException("Nombre de usuario obligatorio");
        if(email == null)  throw new IllegalArgumentException("Email obligatorio");
        if(contrasena == null)  throw new IllegalArgumentException("Contraseña obligatoria");
        this.nombreUsuario = nombreUsuario;
        this.email = email;
        this.contrasena = contrasena;
    }

    public void editarPerfil(String nuevoPerfil){
        this.perfil = nuevoPerfil;
    }

    public void cambiarContrasena(String nuevaContrasena){
        this.contrasena = nuevaContrasena;
    }

    public void comprobarContrasena(String contrasena){
        if(!this.contrasena.equals(contrasena)) throw new IllegalArgumentException("Contraseña incorrecta");
    }

    @Override
    public String toString() {
        return nombreUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public String getEmail() {
        return email;
    }

    public String getPerfil() {
        return perfil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Usuario usuario = (Usuario) o;

        if (!email.equals(usuario.email)) return false;
        if (!nombreUsuario.equals(usuario.nombreUsuario)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nombreUsuario.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
