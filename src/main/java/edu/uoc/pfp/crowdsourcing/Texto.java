package edu.uoc.pfp.crowdsourcing;

public class Texto {
    private final String textoHtml;

    public Texto(String textoHtml) {
        if(textoHtml==null) throw new IllegalArgumentException("Texto HTML obligatorio");
        this.textoHtml = textoHtml;
    }

    public String getTextoHtml() {
        return textoHtml;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Texto texto = (Texto) o;

        if (!textoHtml.equals(texto.textoHtml)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return textoHtml.hashCode();
    }
}
