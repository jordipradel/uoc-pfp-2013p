package edu.uoc.pfp.crowdsourcing;

import java.util.Date;

/** Un calendario que usa la hora actual del sistema operativo */
public class CalendarioSO implements Calendario{
    @Override
    public Fecha hoy() {
        return new Fecha(new Date());
    }
}
