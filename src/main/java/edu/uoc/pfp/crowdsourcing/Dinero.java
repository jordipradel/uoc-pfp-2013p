package edu.uoc.pfp.crowdsourcing;

public class Dinero implements Comparable<Dinero>{
    private final int cantidadEnCentimos;
    private final Moneda moneda;

    public Dinero(int cantidadEnCentimos, Moneda moneda) {
        if(moneda == null) throw new IllegalArgumentException("Moneda incorrecta");
        this.cantidadEnCentimos = cantidadEnCentimos;
        this.moneda = moneda;
    }

    public Dinero suma(Dinero d) {
        if(!d.moneda.equals(this.moneda)) throw new IllegalArgumentException("No se pueden sumar importes en monedas distintas");
        return new Dinero(this.cantidadEnCentimos + d.cantidadEnCentimos,this.moneda);
    }

    public Dinero multiplica(int m) {
        return new Dinero(cantidadEnCentimos * m, moneda);
    }

    public boolean mayorOIgualQue(Dinero d){
        return this.compareTo(d) >= 0;
    }

    @Override
    public int compareTo(Dinero dinero) {
        return new Integer(cantidadEnCentimos).compareTo(dinero.cantidadEnCentimos);
    }

    public String toString(){
        return moneda.format(cantidadEnCentimos);
    }

    public Moneda getMoneda() {
        return moneda;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dinero dinero = (Dinero) o;

        if (cantidadEnCentimos != dinero.cantidadEnCentimos) return false;
        if (!moneda.equals(dinero.moneda)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cantidadEnCentimos;
        result = 31 * result + moneda.hashCode();
        return result;
    }

}
