package edu.uoc.pfp.crowdsourcing;

import java.util.List;

public class ServicioMicroMezenazgo {

    private final RepositorioUsuarios usuarios;
    private final RepositorioProyectos proyectos;
    private final RepositorioSesiones sesiones;
    private final Calendario calendario;

    public ServicioMicroMezenazgo(RepositorioUsuarios usuarios, RepositorioProyectos proyectos, RepositorioSesiones sesiones, Calendario calendario) {
        this.usuarios = usuarios;
        this.proyectos = proyectos;
        this.sesiones = sesiones;
        this.calendario = calendario;
    }

    /** Comprueba la contraseña del usuario y, si es correcta, crea un nuevo identificador de sesión para el mismo
     *  @return El identificador de sesión para usar en los demás métodos
     **/
    public IdSesion login(String nombreUsuario, String contrasena){
        Usuario usuario = usuarios.getUsuario(nombreUsuario);
        if(usuario==null) throw new IllegalArgumentException("No se encuentra el usuario " + nombreUsuario);
        usuario.comprobarContrasena(contrasena);
        return sesiones.nuevaSesion(usuario);
    }

    /** Termina la sesión del usuario  */
    public void logout(IdSesion idSesion){
        sesiones.eliminar(idSesion);
    }

    /** Da de alta una aportación del usuario autenticado en la sesión indicada en el proyecto indicado por la cantdidad indicada */
    public void aportar(IdSesion idSesion, IdProyecto idProyecto, Dinero cantidad){
        Proyecto proyecto = proyectos.getProyecto(idProyecto);
        if(proyecto==null) throw new IllegalArgumentException("No se encuentra el proyecto " + idProyecto);
        Usuario usuario = getUsuario(idSesion);
        proyecto.aportar(usuario, cantidad, calendario.hoy());
    }

    /** Crea un proyecto del usuario autenticado en la sesión indicada con los datos indicados */
    public Proyecto nuevoProeycto(IdSesion idSesion,Dinero objetivo, Fecha fechaInicio, Fecha fechaFin, String titulo, String descripcion){
        Usuario usuario = getUsuario(idSesion);
        return proyectos.nuevoProyecto(usuario,objetivo,fechaInicio,fechaFin,titulo,descripcion);
    }

    public List<ResumenProyecto> getResumenProyectos(String categoria){
        throw new UnsupportedOperationException("Falta implementar");
    }

    /** Edita el título y descripción del proyecto indicado.
     *  El usuario debe ser el autor dek proyecto */
    public void editarProyecto(IdSesion idSesion, IdProyecto idProyecto, String nuevoTitulo, String nuevaDescripcion){
        Usuario usuario = getUsuario(idSesion);
        Proyecto proyecto = proyectos.getProyecto(idProyecto);
        if(proyecto == null) throw new IllegalArgumentException("Proyecto " + idProyecto + " no encontrado");
        proyecto.editar(usuario, nuevoTitulo,nuevaDescripcion);
    }

    /** Retorna el usuario autenticado en la sesión idSesion o lanza una excepción si el identificador de sesion no es válido */
    public Usuario getUsuario(IdSesion idSesion){
        Usuario usuario = sesiones.getUsuario(idSesion);
        if(usuario==null) throw new IllegalArgumentException("No se encuentra la sesión " + idSesion);
        return usuario;
    }


}
