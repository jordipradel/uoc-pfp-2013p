package edu.uoc.pfp.crowdsourcing;

import java.util.Date;

public class Articulo {
    private final String titulo;
    private final Fecha fechaPublicacion;
    private final Texto texto;

    public Articulo(String titulo, Fecha fechaPublicacion, Texto texto) {
        if (titulo == null) throw new IllegalArgumentException("Título obligatorio");
        if (fechaPublicacion == null) throw new IllegalArgumentException("Fecha de publicación obligatoria");
        if (texto == null) throw new IllegalArgumentException("Texto obligatorio");
        this.titulo = titulo;
        this.fechaPublicacion = fechaPublicacion;
        this.texto = texto;
    }

    public String getTitulo() {
        return titulo;
    }

    public Fecha getFechaPublicacion() {
        return fechaPublicacion;
    }

    public Texto getTexto() {
        return texto;
    }

    public String toString(){
        return getTitulo() + "(publicado el " + getFechaPublicacion() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Articulo articulo = (Articulo) o;

        if (!fechaPublicacion.equals(articulo.fechaPublicacion)) return false;
        if (!texto.equals(articulo.texto)) return false;
        if (!titulo.equals(articulo.titulo)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = titulo.hashCode();
        result = 31 * result + fechaPublicacion.hashCode();
        result = 31 * result + texto.hashCode();
        return result;
    }
}
