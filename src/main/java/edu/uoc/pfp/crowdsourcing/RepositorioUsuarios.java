package edu.uoc.pfp.crowdsourcing;

import java.util.HashMap;
import java.util.Map;

public class RepositorioUsuarios {

    private Map<String, Usuario> usuariosPorNombre = new HashMap<String, Usuario>();

    /** Retorna el usuario identificado por nombre */
    public Usuario getUsuario(String nombre){
        if(nombre==null) throw new IllegalArgumentException();
        return usuariosPorNombre.get(nombre);
    }

    /** Da de alta un nuevo usuario con el nombre, email y contraseña indicados */
    public void altaUsuario(String nombre, String email, String contrasena){
        if(getUsuario(nombre)!=null) throw new IllegalArgumentException("Nombre de usuario ya existente");
        usuariosPorNombre.put(nombre,new Usuario(nombre,email,contrasena));
    }
}
